import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import style from './EmployeeLink.css';

const Employee = ({
  id, name, position, departmentTitle
}) => (
  <Link to={`/id_${id}`} className={style.employeeLink}>
    <div className={style.employeeLink__inner}>
      <span>{name}</span>
      <span>{position}</span>
      <span>{departmentTitle}</span>
    </div>
  </Link>
);

Employee.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  departmentTitle: PropTypes.string.isRequired
};

export default Employee;
