import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import style from './Nav.css';

const Nav = ({
  route, children, authorized, signOut
}) => (
  <div className={style.nav}>
    <Link to={`${route}`}>{children}</Link>
    {authorized && (
      <button type="button" onClick={signOut}>
        Sign out
      </button>
    )}
  </div>
);

Nav.propTypes = {
  route: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired
};

export default Nav;
