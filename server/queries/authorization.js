const getDb = require('../db/utils').getDb;

async function authorize(email) {
  const db = await getDb();

  const [[employee]] = await db.get(
    `
    SELECT e.id AS id, IF(e.id, e.password, 'false') AS password
    FROM employees e
    WHERE e.email = $1
  `,
    email
  );

  return employee;
}

exports.authorize = authorize;
